import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'

const firebaseConfig = {
  apiKey: "AIzaSyBcifZR0PjoPLgQDY-5hL_7DSJg9q5qHEg",
  authDomain: "firestore-auth-1-82301.firebaseapp.com",
  projectId: "firestore-auth-1-82301",
  storageBucket: "firestore-auth-1-82301.appspot.com",
  messagingSenderId: "222654247156",
  appId: "1:222654247156:web:b64a7fd5bf79843565aea1"
};

// Initialize Firebase
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

const db = firebase.firestore()
const auth = firebase.auth()
const marcaTiempo = firebase.firestore.FieldValue.serverTimestamp

firebase.getCurrentUser = () => {
  return new Promise((resolve, reject) => {
    const unsuscribe = firebase.auth().onAuthStateChanged(user => {
      unsuscribe()
      resolve(user)
    }, reject)
  })
}

export { db, auth, firebase, marcaTiempo }
